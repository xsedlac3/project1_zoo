//
// Created by Jaromír Sedláček on 26. 1. 2019.
//
#include "Hell.h"

Devil* Hell::getDevil(){
    return new HellishLord(10,10);
}

Devil* Hell::getDevil(){
    return new HellKorse(10,10,10);
}

Devil* Hell::getDevil(){
    return new HellBoy(10,20,10);
}
