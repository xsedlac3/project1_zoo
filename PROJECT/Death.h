//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_DEATH_H
#define PROJECT_DEATH_H

#include "Skeleton.h"

class Death:public Skeleton{
    int m_silaMoru;
    int m_silaSmrti;
    int m_silaStari;
public:
    Death(int silaMoru, int silaSmrti, int silaStari);
    int getAttack();


#endif //PROJECT_DEATH_H
