//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_ANGELHRAD_H
#define PROJECT_ANGELHRAD_H


#include "Hrady.h"

class AngelHrad:public Hrady{
public:
    Angel* getAngel();

};

#endif //PROJECT_ANGELHRAD_H
