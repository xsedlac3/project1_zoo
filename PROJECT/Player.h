//
// Created by Jaromír Sedláček on 28. 1. 2019.
//

#ifndef PROJECT_PLAYER_H
#define PROJECT_PLAYER_H
#include"Hrady.h"
#include"Artifact.h"
#include"Resources.h"


class Player {
protected:
    std::string m_name;
    int m_level;
    int m_attack;
    int m_defense;
    int m_actionPoints;
    std::array<Resources*, 4> m_resources; // vytvorit pocatecni pocet surovin
    std::array<Artifact*, 6> m_artifacts;

public:
    //Prepocty na damage
    //Damage = Stack_Size * random(min_dmg, max_dmg) * [ 1 + 0.05*(A-D) ] A > D
    //Damage = Stack_Size * random(min_dmg, max_dmg) / [ 1 + 0.05*(D-A) ] D > A
    const std::string &getName() const;
    int getLevel() const;
    int getAttack() const;
    int getDefense() const;
    int getActionPoints() const;
    std::array<Resources*, 4> &getResources();
    std::array<Artifact*, 6>& getArtifacts();
    void increaseLevel();
    void goOnAJourney();
    void attackEnemy(Player* enemy, int turnNumber);
    void captureMine();
    void printResources();
    void printInfo();
    virtual ~Player() = 0;
    virtual void hireUnits() = 0;
    virtual void build() = 0;
    virtual void useSpecialSkill() = 0;

private:
    void collectArtifact();
    //ukonceni kola, zacatek kola, atd v enginu delitelnost 7 jako tyden
};


#endif //PROJECT_PLAYER_H
