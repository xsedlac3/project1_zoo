//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_HELLBOY_H
#define PROJECT_HELLBOY_H

#include "Devil.h"

class HellBoy:public Devil{
    int m_silaPlamene;
    int m_silaMentality;
    int m_silaOhnivePesti;
public:
   HellBoy(int silaPlamene, int silaMentality, int silaOhnivePesti );
    int getAttack();
};

#endif //PROJECT_HELLBOY_H
