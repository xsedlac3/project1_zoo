//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_HELL_H
#define PROJECT_HELL_H

#include "Hrady.h"

class Hell:public Hrady{
public:
    Devil* getDevil();

};

#endif //PROJECT_HELL_H
