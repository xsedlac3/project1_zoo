//
// Created by Jaromír Sedláček on 28. 1. 2019.
//
#include "ErrorLog.h"

ErrorLog* ErrorLog::getErrorLog() {
    if (s_errorLog == nullptr) {
        s_errorLog = new ErrorLog();
    }
    return s_errorLog;
}

void ErrorLog::logError(std::string where, std::string what) {
    m_errorMessages.push_back("Destination: " + where + "\nWhat happened: " + what + "\n");
}

std::vector<std::string> ErrorLog::getErrors() {
    return m_errorMessages;
}

ErrorLog::ErrorLog(){}

ErrorLog* ErrorLog::s_errorLog = nullptr;
