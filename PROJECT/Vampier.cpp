//
// Created by Jaromír Sedláček on 26. 1. 2019.
//
#include "Vampier.h"

Vampier::Vampier(int silaOvladnutiMysly, int silaKrve, int silaStari){
    m_silaOvladnutiMysly = silaOvladnutiMysly;
    m_silaKrve = silaKrve;
    m_silaStari = silaStari;
}

int Vampier::getAttack(){
    10*m_silaOvladnutiMysly;
    10*m_silaKrve;
    return 10*m_silaStari;
}
