//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_HELLISHLORD_H
#define PROJECT_HELLISHLORD_H

#include "Devil.h"

class HellishLord:public Devil{
    int m_silaPlamene;
    int m_silaMameni;

public:
    HellishLord(int silaPlamene, int silaMameni );
    int getAttack();
};


#endif //PROJECT_HELLISHLORD_H
