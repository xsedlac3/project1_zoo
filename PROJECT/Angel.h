//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_ANGEL_H
#define PROJECT_ANGEL_H
#include"Jednotky.h"
class Angel{
public:
    virtual	int getAttack() = 0;
    Angel(){}
    virtual ~Angel(){};

#endif //PROJECT_ANGEL_H
