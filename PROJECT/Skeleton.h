//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_SKELETON_H
#define PROJECT_SKELETON_H
#include"Jednotky.h"
class Skeleton{
public:
    virtual	int getAttack() = 0;
    Skeleton(){}
    virtual ~Skeleton(){};

#endif //PROJECT_SKELETON_H
