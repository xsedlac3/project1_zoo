//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_GUARDIANANGEL_H
#define PROJECT_GUARDIANANGEL_H

#include "Angel.h"

class GuardianAngel:public Angel{
    int m_silaKridel;
    int m_silaSpravedlnosti;
    int m_silaOchranehoPole;
public:
    AngelWarrior(int silaKridel, int silaSpravedlnosti, int silaOchranehoPole );
    int getAttack();
};


#endif //PROJECT_GUARDIANANGEL_H
