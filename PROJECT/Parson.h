//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_PARSON_H
#define PROJECT_PARSON_H

#include "Monk.h"

class Parson:public Monk{
    int m_silaModlitby;
    int m_silaRuzence;

public:
    Parson(int silaModlitby, int silaRuzence);
    int getAttack();

#endif //PROJECT_PARSON_H
