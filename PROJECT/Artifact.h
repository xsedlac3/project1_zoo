//
// Created by Jaromír Sedláček on 28. 1. 2019.
//

#ifndef PROJECT_ARTIFACT_H
#define PROJECT_ARTIFACT_H


#include <iostream>

class Artifact {
private:
    std::string m_name;
    int m_attack;
    int m_defense;
    int m_actionPoints;
    int m_position;

public:
    Artifact(const std::string &a_name, const int &a_attack, const int &a_defense, const int &a_actionPoints, const int &a_position);
    const std::string &getName() const;
    int getAttack() const;
    int getDefense() const;
    int getActionPoints() const;
    int getPosition() const;
    void printInfo();
};


#endif //PROJECT_ARTIFACT_H
