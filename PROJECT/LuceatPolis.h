//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_LUCEATPOLIS_H
#define PROJECT_LUCEATPOLIS_H

#include "Hrady.h"

class LuceatPolis:public Hrady{
public:
    Monk* getMonk();

};
#endif //PROJECT_LUCEATPOLIS_H
