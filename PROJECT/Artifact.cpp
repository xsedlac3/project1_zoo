//
// Created by Jaromír Sedláček on 28. 1. 2019.
//
#include "Artifact.h"

Artifact::Artifact(const std::string &a_name, const int &a_attack, const int &a_defense, const int &a_actionPoints, const int &a_position) {
    m_name = a_name;
    m_attack = a_attack;
    m_defense = a_defense;
    m_actionPoints = a_actionPoints;
    m_position = a_position;
}

const std::string &Artifact::getName() const {
    return m_name;
}

int Artifact::getAttack() const {
    return m_attack;
}

int Artifact::getDefense() const {
    return m_defense;
}

int Artifact::getActionPoints() const {
    return m_actionPoints;
}

int Artifact::getPosition() const {
    return m_position;
}

void Artifact::printInfo() {
    std::cout << m_name << "\tAttack: " << m_attack << "  Defense: " << m_defense
              << "  Action points: " << m_actionPoints << std::endl;
}

