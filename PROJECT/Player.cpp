//
// Created by Jaromír Sedláček on 28. 1. 2019.
//
#include "Player.h"

const std::string &Player::getName() const {
    return m_name;
}

std::array<std::pair<int, Creature*>, 3> &Player::getCreatures() {
    return m_creatures;
}

std::array<Resources*, 4>& Player::getResources() {
    return m_resources;
}

std::array<Artifact*, 6>& Player::getArtifacts() {
    return m_artifacts;
}

int Player::getLevel() const {
    return m_level;
}

int Player::getAttack() const {
    return m_attack;
}

int Player::getDefense() const {
    return m_defense;
}

int Player::getActionPoints() const {
    return m_actionPoints;
}

