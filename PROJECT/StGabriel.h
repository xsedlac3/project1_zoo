//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_STGABRIEL_H
#define PROJECT_STGABRIEL_H

#include "Angel.h"

class StGabriel:public Angel{
    int m_silaKridel;
    int m_silaBlesku;
    int m_silaBozihoHnevu;
public:
    AngelWarrior(int silaKridel, int silaBlesku, int silaBozihoHnevu );
    int getAttack();
};


#endif //PROJECT_STGABRIEL_H
