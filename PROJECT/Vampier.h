//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_VAMPIER_H
#define PROJECT_VAMPIER_H

#include "Skeleton.h"

class Vampier:public Skeleton{
    int m_silaOvladnutiMysly;
    int m_silaKrve;
    int m_silaStari;
public:
    Vampier(int silaOvladnutiMysly, int silaKrve, int silaStari);
    int getAttack();

#endif //PROJECT_VAMPIER_H
