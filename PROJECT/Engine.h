//
// Created by Jaromír Sedláček on 28. 1. 2019.
//

#ifndef PROJECT_ENGINE_H
#define PROJECT_ENGINE_H
#include"Player.h"
class Engine {
    std::vector<Player*> m_players;
    int m_day;
    int m_week;
    int m_month;
public:
    Engine();
    int getDay() const;
    int getWeek() const;
    int getMonth() const;
    void createPlayer(std::string &name, int choice);
    void play();
    ~Engine();
};


#endif //PROJECT_ENGINE_H
