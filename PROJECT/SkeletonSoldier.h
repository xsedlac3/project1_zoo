//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_SKELETONSOLDIER_H
#define PROJECT_SKELETONSOLDIER_H

#include "Skeleton.h"

class SkeletonSoldier:public Skeleton{
    int m_silaKosti;
    int m_silaSekery;
public:
    SkeletonSoldier(int silaKosti, int silaSekery);
    int getAttack();
};

#endif //PROJECT_SKELETONSOLDIER_H
