//
// Created by Jaromír Sedláček on 28. 1. 2019.
//

#ifndef PROJECT_JEDNOTKY_H
#define PROJECT_JEDNOTKY_H

class Jednotky{
public:
    virtual	int getAttack() = 0;
    Jednotky(){}
    virtual ~Jednotky(){};

#endif //PROJECT_JEDNOTKY_H
