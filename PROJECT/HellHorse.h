//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_HELLHORSE_H
#define PROJECT_HELLHORSE_H

#include "Devil.h"

class HellHorse:public Devil{
    int m_silaPlamene;
    int m_silaKopyt;
    int m_silaOhnivehoDechu;
public:
    HellHorse(int silaPlamene, int silaKopyt, int silaOhnivehoDechu );
    int getAttack();
};


#endif //PROJECT_HELLHORSE_H
