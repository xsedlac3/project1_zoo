//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_UNDERWORLD_H
#define PROJECT_UNDERWORLD_H

#include "Hrady.h"

class Underworld:public Hrady{
public:
    Devil* getDevil();

};

#endif //PROJECT_UNDERWORLD_H
