//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_MONK_H
#define PROJECT_MONK_H
#include"Jednotky.h"
class Monk{
public:
    virtual	int getAttack() = 0;
    Monk(){}
    virtual ~Monk(){};

#endif //PROJECT_MONK_H
