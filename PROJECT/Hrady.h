//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_HRADY_H
#define PROJECT_HRADY_H


class Hrady{
public:
    virtual Angel* getAngel() = 0;

    virtual Devil* getDevil() = 0;

    virtual Skeleton* getSkeleton() = 0;

    virtual Monk* getMonk() = 0;

};

#endif //PROJECT_HRADY_H