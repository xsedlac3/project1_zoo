//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_DEMONHUNTER_H
#define PROJECT_DEMONHUNTER_H

#include "Monk.h"

class DemonHunter:public Monk{
    int m_silaKrize;
    int m_silaKuse;
    int m_silaSveceneVody;

public:
    DemonHunter(int silaKrize, int silaKuse, int silaSveceneVody);
    int getAttack();


#endif //PROJECT_DEMONHUNTER_H
