//
// Created by Jaromír Sedláček on 28. 1. 2019.
//
#include "Resources.h"

Resources::Resources(std::string a_name, int a_amount) {
    m_name = std::move(a_name);
    m_amount = a_amount;
}

int Resources::getAmount() const {
    return m_amount;
}

void Resources::addAmount(int &amount) {
    if (amount > 0) {
        m_amount += amount;
    } else {
        ErrorLog::getErrorLog()->logError("Add resources", "Amount is invalid.");
    }
}

void Resources::removeAmount(const int &amount) {
    if (amount > 0 && (m_amount - amount) >= 0) {
        m_amount -= amount;
    } else {
        ErrorLog::getErrorLog()->logError("Add resources", "Amount is invalid.");
    }
}

const std::string &Resources::getName() const {
    return m_name;
}

void Resources::printInfo() {
    std::cout << m_name << ": " << m_amount << std::endl;
}

