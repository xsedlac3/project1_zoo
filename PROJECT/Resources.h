//
// Created by Jaromír Sedláček on 28. 1. 2019.
//

#ifndef PROJECT_RESOURCES_H
#define PROJECT_RESOURCES_H

#include <iostream>
#include "ErrorLog.h"

class Resources {
private:
    std::string m_name;
    int m_amount;
public:
    Resources(std::string a_name, int a_amount);
    const std::string &getName() const;
    int getAmount() const;
    void addAmount(int &amount);
    void removeAmount(const int &amount);
    void printInfo();
};


#endif //PROJECT_RESOURCES_H
