//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_ANGELWARRIOR_H
#define PROJECT_ANGELWARRIOR_H

#include "Angel.h"

class AngelWarrior:public Angel{
    int m_silaKridel;
    int m_silaMentality;
public:
    AngelWarrior(int silaKridel, int silaMentality );
    int getAttack();
};

#endif //PROJECT_ANGELWARRIOR_H
