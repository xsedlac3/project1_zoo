//
// Created by Jaromír Sedláček on 28. 1. 2019.
//

#ifndef PROJECT_ERRORLOG_H
#define PROJECT_ERRORLOG_H

#include <iostream>
#include <vector>

class ErrorLog {
    static ErrorLog* s_errorLog;
    std::vector<std::string> m_errorMessages;

public:
    static ErrorLog* getErrorLog();
    void logError(std::string where, std::string what);
    std::vector<std::string> getErrors();

private:
    ErrorLog();
};


#endif //PROJECT_ERRORLOG_H
