//
// Created by Jaromír Sedláček on 28. 1. 2019.
//
#include "Engine.h"

Engine::Engine() {
    m_day = 1;
    m_week = 1;
    m_month = 1;
}

int Engine::getDay() const {
    return m_day;
}

int Engine::getWeek() const {
    return m_week;
}

int Engine::getMonth() const {
    return m_month;
}

void Engine::createPlayer(std::string &name, int choice) {
    if (choice == 0) {
        m_players.push_back(new Human(name));
    } else if (choice == 1) {
        m_players.push_back(new Necromancer(name));
    } else if (choice == 2) {
        m_players.push_back(new Demon(name));
    } else if (choice == 3) {
        m_players.push_back(new Ranger(name));
    }
}

void Engine::play() {}

Engine::~Engine() {
    for (Player* player : m_players) {
        delete player;
    }
}

