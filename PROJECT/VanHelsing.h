//
// Created by Jaromír Sedláček on 26. 1. 2019.
//

#ifndef PROJECT_VANHELSING_H
#define PROJECT_VANHELSING_H

#include "Monk.h"

class VanHelsing:public Monk{
    int m_silaSvatehoBice;
    int m_silaKuse;
    int m_silaViry;

public:
    VanHelsing(int silaSvatehoBice, int silaKuse, int silaViry);
    int getAttack();


#endif //PROJECT_VANHELSING_H
